const publicRoutes = require('./routes/publicRoutes');

// eth networks
const ethRinkebyTestNetwork = {
  websocket_provider: 'wss://rinkeby.infura.io/ws/v3/205d5ce96a0e4ca9a21eb808fa837703',
  provider: 'https://rinkeby.infura.io/v3/205d5ce96a0e4ca9a21eb808fa837703',
  host: 'rinkeby.infura.io',
  gas: 4700036,
  gasPrice: 10000000000,
  networkId: 4
};

const config = {
  migrate: false,
  publicRoutes,
  port: process.env.PORT || '3001',

  ethereum: {
    websocket_provider: ethRinkebyTestNetwork.websocket_provider,
    provider: ethRinkebyTestNetwork.provider,
    host: ethRinkebyTestNetwork.host,

    gas: ethRinkebyTestNetwork.gas,
    gasPrice: ethRinkebyTestNetwork.gasPrice,
    networkId: ethRinkebyTestNetwork.networkId,
    options: {
      defaultBlock: "latest",
      transactionConfirmationBlocks: 1,
      transactionBlockTimeout: 5
    },
    deployer: "0xe61eff5862533842A164eb0C5c99D54d33D81aC9",
    deployerPvtKey: "0x09a369d4af8ff4209bc59c807ba925396ff419c83636da9f053fb09235571845",
    owners: [
      {
        address: '0xA22bd3ce8a0AC474A504E5EB799FBFb4DBA567a0',
        privateKey: '0x0dd885e426c8cc0834d3faa5f50c8141f21411fbcdab009e739b840f6fee7898'
      },
    ]
  }
};

module.exports = config;
