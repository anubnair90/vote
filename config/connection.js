const development = {
  database: 'voting',
  username: 'admin',
  password: 'password',
  host: 'localhost',
  dialect: 'sqlite'
};

module.exports = {
  development
};
