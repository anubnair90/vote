const publicRoutes = {
  'GET /voting/deploy_contract': 'VotingController.createVotingContract',
  'POST /voting/create_proposal': 'VotingController.createVoteProposal',
  'POST /voting/vote': 'VotingController.vote',
  'POST /voting/voting_right': 'VotingController.giveVotingRight',
  'GET /voting/vote_result': 'VotingController.getVoteResult'
};

module.exports = publicRoutes;
