pragma solidity ^0.5.0;
import "@openzeppelin/contracts/math/SafeMath.sol";

/**
 * @title VotingContract
 * @dev This is a voting contract which allow anyone who is in network to create a voting proposal,
 make vote poll on their proposal and get the result of the voting proposal.
 */
contract VotingContract {
    uint256 internal votes_num;
    address public _owner;

    // mapping
    mapping (uint256 => mapping (uint256 => uint256)) internal voteResult;
    mapping (address => uint256) public voters;

    // This is a type for a single proposal.
    struct Proposal {
        string name;   // short name (up to 32 bytes)
        uint256 vote_number; // number of accumulated votes
    }

     // A dynamically-sized array of `Proposal` structs.
    Proposal[] public proposals;

    /*
     *  Events
     */
    event VoteProposalCreated(uint256 indexed _voteId);
    event Vote(uint256 indexed _voteId, uint256 _vote_status_value);
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(isOwner());
        _;
    }

    /**
     * @return true if `msg.sender` is the owner of the contract.
     */
    function isOwner() public view returns (bool) {
        return msg.sender == _owner;
    }

    constructor() public {
        _owner = msg.sender;
        emit OwnershipTransferred(address(0), _owner);
    }

     /**
     * @dev Allows the current owner to transfer control of the contract to a newOwner.
     * @param newOwner The address to transfer ownership to.
     */
    function transferOwnership(address newOwner) public onlyOwner {
        require(newOwner != address(0), "newOwner address is zero address");
        _owner = newOwner;
        emit OwnershipTransferred(address(0), _owner);
    }

    /**
    * @dev get current vote ID
    */
    function getCurrentVoteID() public view returns (uint256){
        return votes_num;
    }

    function giveVotingRight(address voter) public onlyOwner {
        require(voter != address(0), "Sender address is zero address");
        voters[voter] = 1;
    }

    function removeVotingRight(address voter) public onlyOwner {
        require(voter != address(0), "Sender address is zero address");
        voters[voter] = 1;
    }

    /**
    * @dev Create a new Voting Proposal
    * @return the new voteProposal ID
    */
    function createVoteProposal(string memory proposalName) public payable onlyOwner returns (uint256){
        votes_num = SafeMath.add(votes_num, 1);

        // Vote Agree Number
        voteResult[votes_num][0] = 0;
        // Vote Disagree Number
        voteResult[votes_num][1] = 0;
        // Vote Abstain Number
        voteResult[votes_num][2] = 0;
        // Start Voting Time
        voteResult[votes_num][3] = block.timestamp;

        proposals.push(Proposal({
                name: proposalName,
                vote_number: votes_num
        }));


        emit VoteProposalCreated(votes_num);

        return votes_num;
    }

    function getVotingRight(address account) public returns (uint256) {
        return voters[account];
    }

    /**
    * @dev Voting for a given vote ID
    * @param _voteId the given vote ID
    * @param _vote_status_value uint256 the vote of status, 0 Agree, 1 Disagree, 2 Abstain
    */
    function vote(uint256 _voteId, uint256 _vote_status_value) public returns (bool){
        require(_vote_status_value >= 0);
        require(_vote_status_value <= 2);
        // we can vote only for voteId which is created already
        require(_voteId <= votes_num);

        uint voting_right = getVotingRight(msg.sender);
        require(voting_right != 0, "no voting rights");

        if(_vote_status_value == 0)
        {
            voteResult[_voteId][0] = SafeMath.add(voteResult[_voteId][0], voting_right);
        }
        else if(_vote_status_value == 1)
        {
            voteResult[_voteId][1] = SafeMath.add(voteResult[_voteId][1], voting_right);
        }
        else
        {
            voteResult[_voteId][2] = SafeMath.add(voteResult[_voteId][2], voting_right);
        }
        emit Vote(_voteId, _vote_status_value);
        return true;
    }

    /**
    * @dev Gets the voting restult for a vote ID
    * @param _voteId the given vote ID
    * @return the voting restult, true success, false failure
    */
    function getVoteResult(uint256 _voteId) public payable returns (bool){
        uint agree_num = voteResult[_voteId][0];
        uint disagree_num = voteResult[_voteId][1];
        uint abstain_num = voteResult[_voteId][2];

        if(agree_num > disagree_num && agree_num > abstain_num)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}