# Onchain Voting 

Onchain Voting is an app which allows stake holders to vote on voting proposal.
This include a solidity contract which allows to create voting poll by the issuer/admin. The admin can provide voting access to different address, and the individuals can cast their vote on proposals. At the moment the privatekey information is at configuration. But the signing can move to KMS or HSM or we can do simple encryption as mensioned in package.json

## how to compile smart contract?
truffle compile

## How to deploy Run?
docker-compose up

or
NODE_ENV=development node ./api/api.js

## API documentation

https://documenter.getpostman.com/view/9448015/TVCY5X7s
