const Voting = require('../models/Voting');
const Sequelize = require('sequelize');
const Web3Service = require('../../api/services/ethereum/web3');
const path = require('path');
const fs = require('fs');
const async = require('async');
const config = require('../../config/');

const VotingController = () => {
  const createVotingContract = async (req, res) => {
    try {
      const body = req.body;
      return Web3Service.deployContract()
        .then((response) => {
          return res.status(200).json(response);
        });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const giveVotingRight = async (req, res) => {
    try {
      let address = await req.body.destinationAddress;
      let contractAddress = await req.body.contractAddress;
      await Web3Service
      .giveVotingRight(contractAddress, address)
      .then((tx) => {
        return res.status(200).json(tx);
      })
      .catch((err) => {
        return res.status(500).json({ msg: err });
      });
    } catch(err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const createVoteProposal = async (req, res) => {
    try {
      let contractAddress = await req.body.contractAddress;
      let proposalName = await req.body.proposalName;
      await Web3Service
      .createVoteProsal(contractAddress, proposalName)
      .then(async tx => {
        return res.status(200).json(tx);
      })
    } catch(err) {
      console.log(err)
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const vote = async (req, res) => {
    try {
      let contractAddress = await req.body.contractAddress;
      let voteStatus = await req.body.voteStatus;
      let voteId = await req.body.voteId;
      await Web3Service
      .vote(contractAddress, voteId, voteStatus)
      .then((tx) => {
        return res.status(200).json(tx);
      })
      .catch((err) => {
        return res.status(500).json({ msg: err });
      });
    } catch(err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const getVoteResult = async (req, res) => {
    try {
      let contractAddress = await req.body.contractAddress;
      let voteId = await req.body.voteId;
      await Web3Service
      .getVoteResult(contractAddress, voteId)
      .then((tx) => {
        console.log(tx)
        return res.status(200).json(tx);
      })
      .catch((err) => {
        console.log(err)
        return res.status(500).json({ msg: err });
      });
    } catch(err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  return {
    createVotingContract,
    giveVotingRight,
    vote,
    getVoteResult,
    createVoteProposal
  };
};

module.exports = VotingController;
