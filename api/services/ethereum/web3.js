// @flow

const async = require('async');
const config = require('../../../config/');
const fs = require('fs');
const path = require('path');
const Web3 = require('web3');
const URL = require('url').URL;
const https = require('https');

let web3Client = new Web3(new Web3.providers.WebsocketProvider(config.ethereum.websocket_provider));
let abiPath = path.join(__dirname, "contracts", "VotingContract.json");
let abi = JSON.parse(fs.readFileSync(abiPath).toString());

function signTransaction(txABI, contractAddress) {
  const txObj = {
    gas: config.ethereum.gas,
    gasPrice: config.ethereum.gasPrice,
    data: txABI,
    from: config.ethereum.deployer
  };
  if (contractAddress) {
    txObj.to = contractAddress;
  }
  return web3Client.eth.accounts.signTransaction(
    txObj, config.ethereum.deployerPvtKey
  );
}

function submitTransaction(signedTx) {
  return web3Client.eth.sendSignedTransaction(
    signedTx.rawTransaction
  );
}

function signAndSubmitTransaction(txABI, contractAddress) {
  return new Promise((resolve, reject) => {
    signTransaction(txABI, contractAddress)
      .then((signedTx) => {
        return submitTransaction(signedTx);
      })
      .then(res => resolve(res))
      .catch((err) => {
        reject(err);
      });
  });
}

function getContractAddress(txHash) {
  let provider = new URL(config.ethereum.provider);
  let request = (data) => {
    return new Promise((resolve, reject) => {
      const options = {
        hostname: provider.hostname,
        port: provider.port,
        method: 'POST',
        path: provider.pathname,
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(data)
        }
      };
      const req = https.request(options, (res, err) => {
        let str = '';
        res.on('data', (chunk) => {
          str += chunk;
        });
        res.on('end', () => {
          if (err || res.statusCode != 200) {
            reject(err || res.statusCode);
          } else {
            if (str === '') { str = '""'; }
            resolve(JSON.parse(str));
          }
        });
      });
      req.write(data);
      req.end();
    });
  };

  return (cb) => {
    const params = {
      jsonrpc:"2.0",
      method: "eth_getTransactionReceipt",
      params: [txHash],
      id: 1
    };
    return request(JSON.stringify(params))
      .then((res) => {
        if (res.result.contractAddress) {
          const wallet = {
            address: res.result.contractAddress
          };
          return cb(null, wallet);
        } else {
          return cb("null wallet");
        }
      })
      .catch((err) => {
        return cb(err);
      });
  };
}

// callback returns the contract address
function deployContract(argv) {
  const contract = new web3Client.eth.Contract(abi.abi);
  let contractABI;
  if (argv) {
    contractABI = contract.deploy({
      data: abi.bytecode,
      arguments: argv
    }).encodeABI();
  } else{
    contractABI = contract.deploy({
      data: abi.bytecode
    }).encodeABI();
  }

  return new Promise((resolve, reject) => {
    signAndSubmitTransaction(contractABI)
      .then((tx) => {
        let retryableFn = getContractAddress(tx.transactionHash);
        return async.retry(
          {times: 10, interval: 60000},
          retryableFn,
          (err, contract) => {
            if (err) {
              reject(err);
            } else {
              resolve(contract);
            }
          });
      })
      .catch((err) => {
        reject(err);
      });
  });
}


function createVoteProsal(contractAddress, proposalName) {
  let deployedContract = new web3Client.eth.Contract(abi.abi, contractAddress);
  let txABI = deployedContract
      .methods
      .createVoteProposal(
        proposalName
      )
      .encodeABI();

  return new Promise((resolve, reject) => {
    signAndSubmitTransaction(txABI, contractAddress)
      .then((tx) => {
          deployedContract.methods
          .getCurrentVoteID()
          .call()
          .then(async res => {
            resolve(res);
          })
          .catch(err => {
            reject(err);
          });
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function giveVotingRight(contractAddress, address) {
  let deployedContract = new web3Client.eth.Contract(abi.abi, contractAddress);
  let txABI = deployedContract
      .methods
      .giveVotingRight(
        address
      )
      .encodeABI();

  return new Promise((resolve, reject) => {
    signAndSubmitTransaction(txABI, contractAddress)
      .then((tx) => {
        resolve(tx);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function vote(contractAddress, voteId, voteStatus) {
  let deployedContract = new web3Client.eth.Contract(abi.abi, contractAddress);
  let transferTxABI = deployedContract
      .methods
      .vote(voteId, voteStatus)
      .encodeABI();

  let txObj = {
    gas: config.ethereum.gas,
    gasPrice: config.ethereum.gasPrice,
    "to": contractAddress,
    "data": transferTxABI
  };

  return new Promise((resolve, reject) => {
      web3Client.eth.accounts.signTransaction(
        txObj, config.ethereum.owners[0].privateKey
      ).then((signedTx) => {
          submitTransaction(signedTx).then((tx) => {
            resolve(tx);
          }).catch((err) => {
            reject(err);
          });
    }).catch((err) => {
      reject(err);
    });
  });
}

function getTransactionCount(address) {
  return web3Client.eth.getTransactionCount(address);
}

async function getVoteResult(contractAddress, voteId) {
  let deployedContract = new web3Client.eth.Contract(abi.abi, contractAddress);
  return deployedContract.methods
    .getVoteResult(web3Client.utils.numberToHex(voteId))
    .call();
}

function broadcastTransaction(signedTx) {
  const retryCount = 10;
  const retryInterval = 60000;
  const getTransactionReceiptStatus = (txReceipt) => {
    return (cb) => {
      web3Client.eth.getTransactionReceipt(txReceipt, (err, status) => {
        if (err) {
          return cb(err);
        } else {
          if (status == null) {
            return cb(true);
          } else {
            return cb(null, status);
          }
        }
      });
    };
  };

  return new Promise((resolve, reject) => {
    web3Client.eth.sendSignedTransaction(signedTx.rawTransaction, (err, txReceipt) => {
      if (err) {
        reject(err);
      } else {
        let retryableTxReceipt = getTransactionReceiptStatus(txReceipt);
        async.retry({times: retryCount, interval: retryInterval}, retryableTxReceipt, (err, txReceiptStatus) => {
          if (err) {
            handleError(err, 'Error in sendSignedTransaction');
            reject(err);
          } else {
            resolve(txReceiptStatus.status == "0x1");
          }
        });
      }
    });
  });
}

module.exports = {
  web3Client: web3Client,
  deployContract: deployContract,
  createVoteProsal: createVoteProsal,
  giveVotingRight: giveVotingRight,
  vote: vote,
  getVoteResult: getVoteResult
};
