const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

const hooks = {
};

const VotingTable = 'issuance';

const Voting = sequelize.define('Voting', {
  voting_id: {
    type: Sequelize.BIGINT,
  },
  address: {
    type: Sequelize.STRING,
  },
  question: {
    type: Sequelize.STRING,
  },
  status: {
    type: Sequelize.STRING
  },
  details: {
    type: Sequelize.TEXT
  }
}, { hooks, VotingTable });

Voting.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = Voting;
